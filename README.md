## Adivina el password

Define la función `password` que recibe una entrada del usuario y verifica si esta palabra es el password correcto. El usuario solamente tendrá tres oportunidades para adivinar la palabra `python3`. Los mensajes a imprimir serán los siguientes:

- Cuando el usuario no ha encontrado el password: `Only have left`
- Cuando el usuario encontró el password: `Password is valid`
- Cuando el usuario agotó las 3 oportunidades: `Password is invalid`

Considera el siguiente `driver code` como ejemplo:

```python
#driver code  

"""cuando el usuario adivina el password"""
>>Write Password:
asd
Only have left: 2
qwer
Only have left: 1
python3
Password is valid

"""cuando se agotaron las tres oportunidades del usuario"""
>>Write Password:
qwq
Only have left: 2
qwq
Only have left: 1
qwqw
Only have left: 0
Password is invalid
```